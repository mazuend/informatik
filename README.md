**--- Dieser Kurs ist abgeschlossen ---**

# Informatik Übungsstunde

Hier findest du alle Übungsmaterialien und Informationen zu meiner Übungsstunde.
Die Slides und das Handout werde ich jeweils mindestes 24h vor Übungsbeginn hier hochladen.

Meine Übungsstunde findet jemweils am **Mittwoch von 16:00 bis 18:00** via Zoom statt.
Der Zoom-Link ist auf der [Vorlesungsseite](https://lec.inf.ethz.ch/mavt/informatik/2021/).

Falls du Fragen haben solltet, kannst du mir jederzeit schreiben. Ich werde versuchen dir so schnell wie möglich zu antworten.

Mail: [mazuend@student.ethz.ch](mailto:mazuend@student.ethz.ch "just mail me")  oder via Telegram: @pumpeloni


## Folien

 Übung    | Folien            | Folien mit Notizen    | Übungsfragen mit Lösungen | Zusatz |
|:---------|:---------------------|:--------------|:---------------|:----------|
| #1   | [slides_01.pdf](slides/slides_01.pdf)  | [slides_01.pdf](slides/slides_01_n.pdf) | [exercise_01.pdf](exercises/exercise_1.pdf) | [extra_1.pdf](extra/extra_1.pdf) 
| #2    | [slides_02.pdf](slides/slides_02.pdf)  | [slides_02.pdf](slides/slides_02_n.pdf)           | [exercise_02.pdf](exercises/exercise_2.pdf) |
| #3 | [slides_03.pdf](slides/slides_03.pdf) | [slides_03.pdf](slides/slides_03_n.pdf) | [exercise_03.pdf](exercises/exercise_3.pdf) |
| #4 | [slides_04.pdf](slides/slides_04.pdf) | [slides_04.pdf](slides/slides_04_n.pdf) | [exercise_04.pdf](exercises/exercise_4.pdf) |
| #5 | [slides_05.pdf](slides/slides_05.pdf) | [slides_05.pdf](slides/slides_05_n.pdf) | Aufgaben sind in den slides |
| #6 | [slides_06.pdf](slides/slides_06.pdf) | [slides_06.pdf](slides/slides_06_n.pdf) | [exercise_06.pdf](exercises/exercise_6.pdf)|
| #7 | [slides_07.pdf](slides/slides_07.pdf) | [slides_07.pdf](slides/slides_07_n.pdf) | Aufgaben sind in den slides |
| #8 | [slides_08.pdf](slides/slides_08.pdf) | [slides_08.pdf](slides/slides_08_n.pdf) | [exercise_08.pdf](exercises/exercise_8.pdf)|
| #9 | [slides_09.pdf](slides/slides_09.pdf) | [slides_09.pdf](slides/slides_09_n.pdf) | Aufgaben sind in den slides |
| #10 | [slides_10.pdf](slides/slides_10.pdf) | [slides_10.pdf](slides/slides_10_n.pdf) |[exercise_10.pdf](exercises/exercise_10.pdf) |
| #11 | [slides_11.pdf](slides/slides_11.pdf) | [slides_11.pdf](slides/slides_11_n.pdf) | Aufgaben sind in den slides |
| #12 | [slides_12.pdf](slides/slides_12.pdf) | [slides_12.pdf](slides/slides_12_n.pdf) | Aufgaben sind in den slides |
| #13 | [slides_13.pdf](slides/slides_13.pdf) | [slides_13.pdf](slides/slides_13_n.pdf) | Aufgaben sind in den slides |

## PVK

Dieser PVK wurde von Luzian Bieri für das FS 2020 verfasst. Der Vorlesungsinhalt ist derselbe wie in diesem Semester (FS 2021).

Titel    | PDF          | 
|:---------|:---------------------|
| PVK-Skript komplett   | [pvk_info_komplett.pdf](PVK/pvk_info_komplett.pdf)  |
| PVK-Skript Theorie | [pvk_info_theorie.pdf](PVK/pvk_info_theorie.pdf) |
| PVK-Skript Theorie ohne Beispiele | [pvk_info_theorie_ohne_beispiele.pdf](PVK/pvk_info_theorie_ohne_beispiele.pdf) |
| PVK-Skript Aufgaben | [pvk_info_aufgaben.pdf](PVK/pvk_info_aufgaben.pdf) |
| PVK-Skript Aufgaben Druckversion | [pvk_info_aufgaben_druckbar.pdf](PVK/pvk_info_aufgaben_druckbar.pdf) |

## YouTube - Informatik mini-Serie

Hast du ein Konzept nicht ganz verstanden oder möchtest dein Wissen dazu auffrischen? Du bist kein Fan vom PVK-Skript? Die ETH hat dazu eine YouTube mini-Serie produziert, in der dir wichtige Konzepte erklärt werden. 

[Link zur mini-Serie](https://www.youtube.com/playlist?list=PL0WLWTsoAENg6yiw2hdDDzu-6xpZhU-yh)

## Zusammenfassung

Hier ist meine Zusammenfassung für Informatik. Der Inhalt sollte korrekt sein, jedoch übernehme ich keine Garantie dafür.

Als Basis nutze ich die Zusammenfassung von Luzian Bieri.
Solltest du einen Fehler finden, bitte ich dich diesen mir zu melden. Vielen Dank!

Titel    | PDF          | Word |
|:---------|:---------------------|:-----------------|
| Meine Zusammenfassung   | [Zusammenfassung_Info_FS_2020.pdf](zusammenfassung/Zusammenfassung_Info_FS_2020.pdf)  | [Zusammenfassung_Info_FS_2020.docx](zusammenfassung/Zusammenfassung_Info_FS_2020.docx)|

Weitere Zusammenfassungen findest du auf der [AMIV-Seite](https://amiv.ethz.ch/de/studies/documents/).
